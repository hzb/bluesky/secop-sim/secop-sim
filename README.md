# SECoP-sim

This repo contains containeraized SEC Nodes that simulate Hardware needed for the ROCK-IT Project

Gas Dosing (Node):
- massflow_contr1    
- massflow_contr2
- massflow_contr3
- backpressure_contr1

Reactor Cell (Node):
- temperature_reg  
- temperature_sam

## Prerequisites
This demo is intended to run on a linux machine with `make` and `docker` installed.

## Getting started

The [Demo](GasDosing_demo.ipynb) shows generation of Ophyd devices upon connection to a SEC Node and also basic plan execution

The SEC Nodes and a frappy-gui can be started via:

    make frappy

